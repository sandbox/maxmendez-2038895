<?php

/**
 * @file
 * Contains the ResponsiveCarousel style plugin.
 */

/**
 * Style plugin to render each item in an ordered or unordered list.
 *
 * @ingroup views_style_plugins
 */
class responsive_carousel_style_plugin extends views_plugin_style {

  function option_definition() {
    $options = parent::option_definition();
    $options['duration'] = array('default' => 1500);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['duration'] = array(
      '#type' => 'textfield',
      '#title' => t('Duration'),
      '#default_value' => $this->options['duration'],
      '#description' => t('Duration in milliseconds of the animation'),
    );

  }

  function validate() {
    $errors = parent::validate();

    return $errors;
  }

}
