<?php

/**
 * @file
 * Views integration for Responsive Carousel module.
 */

/**
 * Implements hook_views_plugin().
 */
function responsive_carousel_views_plugins() {
  $plugins['style']['responsive_carousel'] = array(
    'title' => t('Responsive Carousel'),
    'help' => t('Display rows in a carousel via Responsive Carousel.'),
    'handler' => 'responsive_carousel_style_plugin',
    'path' => drupal_get_path('module', 'responsive_carousel') . '/includes',
    'theme' => 'responsivecarousel_view',
    'theme path' => drupal_get_path('module', 'responsive_carousel') . '/includes',
    'uses row plugin' => TRUE,
    'uses options' => TRUE,
    'uses grouping' => FALSE,
    'type' => 'normal',
  );
  return $plugins;
}


/**
 * Preprocess function for jcarousel-view.tpl.php.
 */
function template_preprocess_responsivecarousel_view(&$variables) {
  
  global $index_carousel;

  //Get view
  $view = $variables['view'];

  //Index Manage
  $index_carousel = isset($index_carousel) ? $index_carousel : 0;
  $index_carousel++;

  //Create new class
  $class = 'responsive-carousel-id-' . $index_carousel;
  $variables['classes_array'][] = $class;

  // Load CSS and JS Files
  drupal_add_css(drupal_get_path('module', 'responsive_carousel') . '/css/responsive_carousel.css');
  drupal_add_js(
    drupal_get_path('module', 'responsive_carousel') . '/js/responsive_carousel.js',
    array('type' => 'file', 'scope' => 'footer')
  );

  //Create Behavior
  $rCarousel = '
    (function ($) {
      Drupal.behaviors.responsiveCarousel' . $index_carousel . ' = {
        attach: function (context, settings) {
          $(".' . $class . '").responsiveCarousel({duration : ' . $view->style_plugin->options['duration'] . '});
        }
      };
    })(jQuery);
  ';

  drupal_add_js(
    $rCarousel,
    array(
      'type' => 'inline',
      'scope' => 'footer'
    )
  );
  
}
