
##Description
Views's plugin, to create a basic carousel responsive, which automatically calculate the offset to the width of its container and considering margins and paddings. Also recalculated their values ​​with each change of the window size, allowing dynamically respond to mobile devices.  
For more information about ResponsiveCarousel jQuery Plugin, visit the official project:
https://github.com/Akiracr/responsiveCarousel


##Installation
1) Place this module directory in your modules folder (this will usually be
   "sites/all/modules/").

2) Enable the module within your Drupal site at Administer -> Site Building ->
   Modules (admin/build/modules).

##Usage


##API Usage


##Authors

Max Mendez (http://maxmendez.net)
